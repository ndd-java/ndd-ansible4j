# Ansible4J

Ansible4J is a thin Java wrapper around [Ansible](http://www.ansible.com/). Ansible4J spawns a process using the native implementation because some Ansible dependencies are not available in Jython. Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

### Ansible

```java
// arguments only
Ansible ansible1 = new Ansible();
ansible1.getOptions().setInventory(inventoryUrl.getPath());
ansible1.getOptions().setPrivateKey(privateKeyFile.getPath());
ansible1.getOptions().setRemoteUser("ansible4j");

ansible1.getOptions().setArguments("ls -l /home");
AnsibleExitCode exitCode1 = ansible1.runAndWait("valid_hosts");
assertThat(exitCode1).isEqualTo(AnsibleExitCode.SUCCESS);

// module only
Ansible ansible2 = new Ansible();
ansible2.getOptions().setInventory(inventoryUrl.getPath());
ansible2.getOptions().setPrivateKey(privateKeyFile.getPath());
ansible2.getOptions().setRemoteUser("ansible4j");

ansible2.getOptions().setModule("ping");
AnsibleExitCode exitCode2 = ansible2.runAndWait("valid_hosts");
assertThat(exitCode2).isEqualTo(AnsibleExitCode.SUCCESS);

// module and arguments
Ansible ansible3 = new Ansible();
ansible3.getOptions().setInventory(inventoryUrl.getPath());
ansible3.getOptions().setPrivateKey(privateKeyFile.getPath());
ansible3.getOptions().setRemoteUser("ansible4j");

ansible3.getOptions().setModule("service");
ansible3.getOptions().setArguments("name=apache2 state=started");
AnsibleExitCode exitCode3 = ansible3.runAndWait("valid_hosts");
assertThat(exitCode3).isEqualTo(AnsibleExitCode.SUCCESS);
```

### Ansible Playbook

```java
AnsiblePlaybook playbook1 = new AnsiblePlaybook();
playbook1.getOptions().setInventory(inventoryUrl.getPath());
playbook1.getOptions().setPrivateKey(privateKeyFile.getPath());
playbook1.getOptions().setRemoteUser("ansible4j");

AnsibleExitCode exitCode11 = playbook1.runAndWait(new File(playbookUrl.getPath()));
assertThat(exitCode11).isEqualTo(AnsibleExitCode.SUCCESS);
```

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-ansible4j</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-ansible4j/project-reports.html](https://ndd-java.bitbucket.org/ndd-ansible4j/project-reports.html)

## About

Copyright David DIDIER 2014
