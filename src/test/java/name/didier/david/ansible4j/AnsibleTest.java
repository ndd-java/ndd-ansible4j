package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.ansible4j.AnsibleVerbosity.HIGHEST;
import static name.didier.david.test4j.testng.TestNgGroups.INTEGRATION;

import java.io.File;
import java.net.URL;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgResourceListener;
import name.didier.david.test4j.testng.TestResource;

@Test(groups = INTEGRATION)
@Listeners(TestNgResourceListener.class)
public class AnsibleTest {

    @TestResource("docker/files")
    private URL dockerFilesUrl;
    @TestResource("docker/files/usr/local/ssh/without-password.rsa")
    private URL privateKeyUrl;
    @TestResource("inventory.txt")
    private URL inventoryUrl;

    /** Used to speed up manual testing only. Should be false. */
    private final boolean externalDocker = false;

    private Ansible4jDockerManager manager;
    private Ansible ansible;

    @BeforeClass
    public void beforeClass() {
        if (externalDocker) {
            return;
        }
        manager = new Ansible4jDockerManager();
        manager.connect();
        manager.startContainer();
    }

    @BeforeMethod
    public void create_ansible() {
        File privateKeyFile = new File(privateKeyUrl.getPath());
        Ansible4jTestUtils.setPrivate(privateKeyFile);

        ansible = new Ansible();
        ansible.getOptions().setInventory(inventoryUrl.getPath());
        ansible.getOptions().setRemoteUser("ansible4j");
        ansible.getOptions().setPrivateKey(privateKeyUrl.getPath());
        ansible.getOptions().setVerbosity(HIGHEST);
    }

    @AfterClass
    public void afterClass() {
        if (externalDocker) {
            return;
        }
        manager.stopContainer();
        manager.close();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void ansible_should_return_success_for_a_valid_command() {
        ansible.getOptions().setArguments("ls -l /home");
        AnsibleExitCode code = ansible.runAndWait("valid_hosts");
        assertThat(code).isEqualTo(AnsibleExitCode.SUCCESS);
    }

    public void ansible_should_return_failed_for_an_invalid_command() {
        ansible.getOptions().setArguments("ls -l /invalid");
        AnsibleExitCode code = ansible.runAndWait("valid_hosts");
        assertThat(code).isEqualTo(AnsibleExitCode.FAILED);
    }

    public void ansible_should_return_dark_for_an_invalid_host() {
        ansible.getOptions().setArguments("ls -l /home");
        AnsibleExitCode code = ansible.runAndWait("dark_hosts");
        assertThat(code).isEqualTo(AnsibleExitCode.DARK);
    }
}
