package name.didier.david.ansible4j.documentation;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import name.didier.david.ansible4j.Ansible;
import name.didier.david.ansible4j.Ansible4jDockerManager;
import name.didier.david.ansible4j.Ansible4jTestUtils;
import name.didier.david.ansible4j.AnsibleExitCode;
import name.didier.david.ansible4j.AnsiblePlaybook;

/**
 * Validates the documentation code.
 *
 * @author ddidier
 */
@SuppressWarnings("all")
public class Documentation {

    public static void main(String[] args)
            throws IOException {
        URL dockerFilesUrl = Ansible4jTestUtils.findResource("docker/files");
        URL privateKeyUrl = Ansible4jTestUtils.findResource("docker/files/usr/local/ssh/without-password.rsa");
        URL inventoryUrl = Ansible4jTestUtils.findResource("inventory.txt");
        URL playbookUrl = Ansible4jTestUtils.findResource("ansible/playbook.yml");

        File privateKeyFile = new File(privateKeyUrl.getPath());
        Ansible4jTestUtils.setPrivate(privateKeyFile);

        Ansible4jDockerManager manager = new Ansible4jDockerManager();
        try {
            manager.connect();
            manager.startContainer();

            // ---------------------------------------------------------------------------------------------------------

            // arguments only
            Ansible ansible1 = new Ansible();
            ansible1.getOptions().setInventory(inventoryUrl.getPath());
            ansible1.getOptions().setPrivateKey(privateKeyFile.getPath());
            ansible1.getOptions().setRemoteUser("ansible4j");

            ansible1.getOptions().setArguments("ls -l /home");
            AnsibleExitCode exitCode1 = ansible1.runAndWait("valid_hosts");
            assertThat(exitCode1).isEqualTo(AnsibleExitCode.SUCCESS);

            // module only
            Ansible ansible2 = new Ansible();
            ansible2.getOptions().setInventory(inventoryUrl.getPath());
            ansible2.getOptions().setPrivateKey(privateKeyFile.getPath());
            ansible2.getOptions().setRemoteUser("ansible4j");

            ansible2.getOptions().setModule("ping");
            AnsibleExitCode exitCode2 = ansible2.runAndWait("valid_hosts");
            assertThat(exitCode2).isEqualTo(AnsibleExitCode.SUCCESS);

            // module and arguments
            Ansible ansible3 = new Ansible();
            ansible3.getOptions().setInventory(inventoryUrl.getPath());
            ansible3.getOptions().setPrivateKey(privateKeyFile.getPath());
            ansible3.getOptions().setRemoteUser("ansible4j");

            ansible3.getOptions().setModule("service");
            ansible3.getOptions().setArguments("name=apache2 state=started");
            AnsibleExitCode exitCode3 = ansible3.runAndWait("valid_hosts");
            assertThat(exitCode3).isEqualTo(AnsibleExitCode.SUCCESS);

            // ---------------------------------------------------------------------------------------------------------

            AnsiblePlaybook playbook1 = new AnsiblePlaybook();
            playbook1.getOptions().setInventory(inventoryUrl.getPath());
            playbook1.getOptions().setPrivateKey(privateKeyFile.getPath());
            playbook1.getOptions().setRemoteUser("ansible4j");

            AnsibleExitCode exitCode11 = playbook1.runAndWait(new File(playbookUrl.getPath()));
            assertThat(exitCode11).isEqualTo(AnsibleExitCode.SUCCESS);
        } finally {
            manager.stopContainer();
            manager.close();
        }
    }
}
