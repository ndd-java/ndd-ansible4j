package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(groups = UNIT)
public class AnsiblePlaybookOptionsTest
        extends AbstractAnsibleOptionsTestBase {

    private AnsiblePlaybookOptions options;

    @Override
    @BeforeMethod
    public void create_options() {
        super.create_options();
        this.options = newAnsibleOptions();
    }

    // -----------------------------------------------------------------------------------------------------------------

    // @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    // public void setArguments_should_reject_blank_values(String arguments) {
    // try {
    // options.setArguments(arguments);
    // failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
    // } catch (IllegalArgumentException e) {
    // assertThat(e).hasMessageContaining("arguments");
    // }
    // }
    //
    // public void setArguments_should_accept_non_blank_values() {
    // options.setArguments("some args");
    // assertThat(options.getArguments()).isEqualTo("some args");
    // }
    //
    // public void toArray_should_return_arguments_as_an_array_with_arguments_set() {
    // options.setArguments("some args");
    // assertThat(options.toArray()).containsExactly("-a", "some args");
    // }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public void toArray_should_return_arguments_as_an_array_with_all_set() {
        options.setParallelism(1);
        options.setInventory("/path/to/inventory");
        options.setPrivateKey("/path/to/private_key");
        options.setRemoteUser("remote_user");

        // @formatter:off
        assertThat(options.toArray()).containsExactly(
                "-f", "1",
                "-i", "/path/to/inventory",
                "--private-key", "/path/to/private_key",
                "-u", "remote_user");
        // @formatter:on
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public void toString_should_return_a_multi_lines_representation() {
        options.setParallelism(1);
        options.setInventory("/path/to/inventory");
        options.setPrivateKey("/path/to/private_key");
        options.setRemoteUser("remote_user");
        options.setVerbosity(AnsibleVerbosity.HIGHEST);

        // @formatter:off
        assertThat(options.toString()).matches(
                "name.didier.david.ansible4j.AnsiblePlaybookOptions@[0-9a-fA-F]+\\[\n"
              + "  inventory=/path/to/inventory\n"
              + "  parallelism=1\n"
              + "  privateKey=/path/to/private_key\n"
              + "  remoteUser=remote_user\n"
              + "  verbosity=vvvv\n"
              + "\\]");
        // @formatter:on
    }

    @Override
    protected AnsiblePlaybookOptions newAnsibleOptions() {
        return new AnsiblePlaybookOptions();
    }
}
