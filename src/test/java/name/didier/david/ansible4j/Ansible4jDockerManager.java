package name.didier.david.ansible4j;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.net.URL;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;

import name.didier.david.ansible4j.docker.AbstractDockerManager;
import name.didier.david.test4j.testng.DefaultTestResourceResolutionStrategy;
import name.didier.david.test4j.testng.TestResourceResolutionStrategy;

/**
 * Manage the Ansible4J Docker image using the REST API.
 *
 * @author ddidier
 */
public class Ansible4jDockerManager
        extends AbstractDockerManager {

    public static final int HOST_HTTP_PORT = 11080;
    public static final int HOST_SSH_PORT = 11022;
    public static final int GUEST_HTTP_PORT = 80;
    public static final int GUEST_SSH_PORT = 22;

    public static final File DOCKER_FILE;
    public static final String CONTAINER_NAME = "ndd-ansible4j";
    public static final String IMAGE_NAME = "ddidier/ndd-ansible4j";

    static {
        TestResourceResolutionStrategy strategy = new DefaultTestResourceResolutionStrategy();
        URL dockerUrl = strategy.findResource(Ansible4jDockerManager.class, newArrayList("docker/Dockerfile"));
        DOCKER_FILE = new File(dockerUrl.getFile());
    }

    private ExposedPort tcp22;
    private ExposedPort tcp80;

    /**
     * Constructor using the default Docker URL (http://{@value #DEFAULT_HOST}:{@value #DEFAULT_PORT}).
     */
    public Ansible4jDockerManager() {
        super(DOCKER_FILE, IMAGE_NAME);
    }

    /**
     * Constructor using the given Docker URL.
     *
     * @param host the host of the docker REST service.
     * @param port the port of the docker REST service.
     */
    public Ansible4jDockerManager(String host, int port) {
        super(DOCKER_FILE, IMAGE_NAME, host, port);
    }

    @Override
    protected String doCreateContainer() {
        tcp22 = ExposedPort.tcp(GUEST_SSH_PORT);
        tcp80 = ExposedPort.tcp(GUEST_HTTP_PORT);
        return dockerClient().createContainerCmd(imageTag).withExposedPorts(tcp22, tcp80).exec().getId();
    }

    @Override
    protected void doStartContainer() {
        Ports portBindings = new Ports();
        portBindings.bind(tcp22, Ports.Binding(HOST_SSH_PORT));
        portBindings.bind(tcp80, Ports.Binding(HOST_HTTP_PORT));
        dockerClient().startContainerCmd(containerId()).withPortBindings(portBindings).exec();
    }
}
