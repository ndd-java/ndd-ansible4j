package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.test4j.testng.TestNgDataProviders.BLANK_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.NEGATIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgDataProviders.STRICTLY_POSITIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgDataProviders;

@Test(groups = UNIT)
public abstract class AbstractAnsibleOptionsTestBase {

    private AbstractAnsibleOptions options;

    @BeforeMethod
    public void create_options() {
        this.options = newAnsibleOptions();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    public void setInventory_should_reject_blank_values(String inventory) {
        try {
            options.setInventory(inventory);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("inventory");
        }
    }

    public void setInventory_should_accept_non_blank_values() {
        options.setInventory("/path/to/inventory");
        assertThat(options.getInventory()).isEqualTo("/path/to/inventory");
    }

    public void toArray_should_return_inventory_as_an_array_with_arguments_set() {
        options.setInventory("/path/to/inventory");
        assertThat(options.toArray()).containsExactly("-i", "/path/to/inventory");
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NEGATIVE_NUMBERS)
    public void setParallelism_should_reject_negative_values(Integer parallelism) {
        try {
            options.setParallelism(parallelism);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("parallelism");
        }
    }

    public void setParallelism_should_reject_null() {
        try {
            options.setParallelism(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("parallelism");
        }
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = STRICTLY_POSITIVE_NUMBERS)
    public void setParallelism_should_accept_positive_values(Integer parallelism) {
        options.setParallelism(parallelism);
        assertThat(options.getParallelism()).isEqualTo(parallelism);
    }

    public void toArray_should_return_arguments_as_an_array_with_parallelism_set() {
        options.setParallelism(1);
        assertThat(options.toArray()).containsExactly("-f", "1");
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    public void setPrivateKey_should_reject_blank_values(String privateKey) {
        try {
            options.setPrivateKey(privateKey);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("privateKey");
        }
    }

    public void setPrivateKey_should_accept_non_blank_values() {
        options.setPrivateKey("/path/to/private_key");
        assertThat(options.getPrivateKey()).isEqualTo("/path/to/private_key");
    }

    public void toArray_should_return_arguments_as_an_array_with_privateKey_set() {
        options.setPrivateKey("/path/to/private_key");
        assertThat(options.toArray()).containsExactly("--private-key", "/path/to/private_key");
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    public void setRemoteUser_should_reject_blank_values(String remoteUser) {
        try {
            options.setRemoteUser(remoteUser);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("remoteUser");
        }
    }

    public void setRemoteUser_should_accept_non_blank_values() {
        options.setRemoteUser("remote_user");
        assertThat(options.getRemoteUser()).isEqualTo("remote_user");
    }

    public void toArray_should_return_arguments_as_an_array_with_remoteUser_set() {
        options.setRemoteUser("remote_user");
        assertThat(options.toArray()).containsExactly("-u", "remote_user");
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void setVerbosity_should_reject_null_value() {
        try {
            options.setVerbosity(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("verbosity");
        }
    }

    public void setVerbosity_should_accept_non_null_values() {
        options.setVerbosity(AnsibleVerbosity.LOWEST);
        assertThat(options.getVerbosity()).isEqualTo(AnsibleVerbosity.LOWEST);
        options.setVerbosity(AnsibleVerbosity.HIGHEST);
        assertThat(options.getVerbosity()).isEqualTo(AnsibleVerbosity.HIGHEST);
    }

    public void toArray_should_return_arguments_as_an_array_with_verbosity_set() {
        options.setVerbosity(AnsibleVerbosity.LOW);
        assertThat(options.toArray()).containsExactly("-vv");
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void toArray_should_return_arguments_as_an_array_with_defaults_only() {
        assertThat(options.toArray()).containsExactly();
    }

    public abstract void toArray_should_return_arguments_as_an_array_with_all_set();

    // -----------------------------------------------------------------------------------------------------------------

    public abstract void toString_should_return_a_multi_lines_representation();

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract AbstractAnsibleOptions newAnsibleOptions();
}
