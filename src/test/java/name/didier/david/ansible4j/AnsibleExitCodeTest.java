package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

@Test(groups = UNIT)
public class AnsibleExitCodeTest {

    public void valueOf_should_return_the_exit_code_for_a_valid_index() {
        assertThat(AnsibleExitCode.valueOf(0)).isEqualTo(AnsibleExitCode.SUCCESS);
        assertThat(AnsibleExitCode.valueOf(1)).isEqualTo(AnsibleExitCode.ERROR);
        assertThat(AnsibleExitCode.valueOf(2)).isEqualTo(AnsibleExitCode.FAILED);
        assertThat(AnsibleExitCode.valueOf(3)).isEqualTo(AnsibleExitCode.DARK);
    }

    public void valueOf_should_raise_an_exception_for_an_invalid_index() {
        try {
            AnsibleExitCode.valueOf(-1);
            failBecauseExceptionWasNotThrown(ArrayIndexOutOfBoundsException.class);
        } catch (ArrayIndexOutOfBoundsException e) {
            // expected
        }

        try {
            AnsibleExitCode.valueOf(4);
            failBecauseExceptionWasNotThrown(ArrayIndexOutOfBoundsException.class);
        } catch (ArrayIndexOutOfBoundsException e) {
            // expected
        }
    }
}
