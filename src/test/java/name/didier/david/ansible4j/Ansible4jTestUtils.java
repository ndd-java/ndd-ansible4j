package name.didier.david.ansible4j;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.net.URL;

import name.didier.david.test4j.testng.DefaultTestResourceResolutionStrategy;
import name.didier.david.test4j.testng.TestResourceResolutionStrategy;

public final class Ansible4jTestUtils {

    private static final TestResourceResolutionStrategy STRATEGY = new DefaultTestResourceResolutionStrategy();

    /** Utility class. */
    private Ansible4jTestUtils() {
        super();
    }

    public static URL findResource(String resourceName) {
        return STRATEGY.findResource(Ansible4jTestUtils.class, newArrayList(resourceName));
    }

    /**
     * Restrict the file rights to be used by SSH.
     *
     * @param file the private key file.
     */
    public static void setPrivate(File file) {
        file.setReadable(false, false);
        file.setWritable(false, false);
        file.setExecutable(false, false);

        file.setReadable(true, true);
        file.setWritable(true, true);
        // file.setExecutable(true, true);
    }
}
