package name.didier.david.ansible4j.docker;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;
import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;
import static name.didier.david.check4j.api.ConciseCheckers.checkStrictlyPositive;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.Scanner;
import java.util.logging.Level;

import javax.ws.rs.ProcessingException;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.BuildImageCmd;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;

/**
 * <p>
 * Manage Docker using the REST API.
 * </p>
 * <p>
 * The TCP connection must be enabled. On Ubuntu, edit the file <code>/etc/default/docker</code> and add <code>
 * DOCKER_OPTS='-H tcp://{}:{} -H unix:///var/run/docker.sock'</code>.
 * </p>
 *
 * @author ddidier
 */
public abstract class AbstractDockerManager
        implements DockerManager {

    /** The default Docker host. */
    public static final String DEFAULT_HOST = "localhost";
    /** The default Docker port. */
    public static final int DEFAULT_PORT = 2375;

    /** The associated logger. */
    protected final Logger logger;
    /** The URL of the Docker REST service. */
    protected final String dockerUrl;
    /** The {@code Dockerfile} used to build the image. */
    protected File dockerFile;
    /** The tag of the image to use. */
    protected final String imageTag;

    /** The client of the Docker REST service. */
    private DockerClient dockerClient;
    /** The ID of the container using the image. */
    private String containerId;

    /**
     * Constructor using the default Docker URL (http://{@value #DEFAULT_HOST}:{@value #DEFAULT_PORT}).
     *
     * @param dockerFile the {@code Dockerfile} used to build the image.
     * @param imageTag the tag of the image to use.
     */
    protected AbstractDockerManager(File dockerFile, String imageTag) {
        this(dockerFile, imageTag, DEFAULT_HOST, DEFAULT_PORT);
    }

    /**
     * Constructor using the given Docker URL.
     *
     * @param dockerFile the {@code Dockerfile} used to build the image.
     * @param imageTag the tag of the image to use.
     * @param host the host of the docker REST service.
     * @param port the port of the docker REST service.
     */
    protected AbstractDockerManager(File dockerFile, String imageTag, String host, int port) {
        super();
        this.logger = LoggerFactory.getLogger(getClass());
        this.dockerFile = checkNotNull(dockerFile, "dockerFile");
        this.imageTag = checkNotBlank(imageTag, "imageTag");
        this.dockerUrl = format("http://%s:%d", checkNotBlank(host, "host"), checkStrictlyPositive(port, "port"));
        bridgeJul();
    }

    @Override
    public void connect() {
        checkNotConnected();

        try {
            logger.debug("Connecting to Docker using REST at {}", dockerUrl);
            dockerClient = DockerClientBuilder.getInstance(dockerUrl).build();
            dockerClient.pingCmd().exec();
            logger.info("Successfully connected to Docker at {}", dockerUrl);
        } catch (ProcessingException e) {
            if (e.getCause() instanceof ConnectException) {
                logger.error("Error while connecting to Docker using REST at {}\n"
                        + "  Have you enabled the TCP connection?\n"
                        + "  On Ubuntu, edit the file '/etc/default/docker' and add:\n"
                        + "    DOCKER_OPTS='-H tcp://{}:{} -H unix:///var/run/docker.sock'", dockerUrl, DEFAULT_HOST,
                        DEFAULT_PORT);
            }
            throw new RuntimeException("Error while connecting to " + dockerUrl, e);
        }
    }

    @Override
    public void startContainer() {
        if (!hasImageTaggedWith(latestImageTag())) {
            buildImageAndTag(dockerFile.getParentFile(), latestImageTag());
        }

        containerId = doCreateContainer();
        doStartContainer();
    }

    @Override
    public void stopContainer() {
        logger.debug("Stopping container {}", containerId);
        dockerClient.stopContainerCmd(containerId).exec();
        logger.info("Successfully stopped container {}", containerId);
    }

    // @Override
    // public boolean isContainerRunning() {
    // Image image = getImageTaggedWith(latestImageTag());
    // if (image == null) {
    // return false;
    // }
    //
    // List<Container> containers = dockerClient.listContainersCmd().exec();
    // for (Container container : containers) {
    // System.out.println(container.getImage());
    // }
    // return false;
    // }

    @Override
    public void close() {
        if (dockerClient != null) {
            try {
                logger.debug("Closing Docker client at {}", dockerUrl);
                dockerClient.close();
                logger.info("Successfully closed Docker client at {}", dockerUrl);
            } catch (IOException e) {
                throw new RuntimeException("Error while closing client of " + dockerUrl, e);
            }
        }
    }

    @Override
    public File getDockerFile() {
        return dockerFile;
    }

    @Override
    public String getImageTag() {
        return imageTag;
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected DockerClient dockerClient() {
        return dockerClient;
    }

    protected String containerId() {
        return containerId;
    }

    protected String latestImageTag() {
        return imageTag + ":latest";
    }

    /**
     * Search for an already built image with the given tag.
     *
     * @param tag the tag to search for.
     * @return the image with the given tag if it exists, {@code null} otherwise.
     */
    protected Image getImageTaggedWith(String tag) {
        checkConnected();
        logger.info("Looking for image tagged with '{}'", tag);

        for (Image image : dockerClient.listImagesCmd().exec()) {
            logger.debug("Image {} is tagged with '{}'", image.getId(), image.getRepoTags());
            if (ArrayUtils.contains(image.getRepoTags(), tag)) {
                logger.info("Found image tagged with '{}'", tag);
                return image;
            }
        }
        logger.info("Found no image tagged with '{}'", tag);
        return null;
    }

    /**
     * Search for an already built image with the given tag.
     *
     * @param tag the tag to search for.
     * @return <code>true</code> if there is an image with the given tag, <code>false</code> otherwise.
     */
    protected boolean hasImageTaggedWith(String tag) {
        return getImageTaggedWith(tag) != null;
    }

    /**
     * Build an image then tag it.
     *
     * @param baseDirectory the directory containing the {@code Dockerfile}
     * @param tag the tag to apply.
     */
    protected void buildImageAndTag(File baseDirectory, String tag) {
        logger.info("Building image from '{}'", baseDirectory);

        try (BuildImageCmd buildCommand = dockerClient.buildImageCmd(baseDirectory).withTag(tag)) {
            try (InputStream response = buildCommand.exec()) {
                try (Scanner scanner = new Scanner(response, UTF_8.name())) {
                    while (scanner.hasNext()) {
                        logger.debug(scanner.nextLine());
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error while building image from " + baseDirectory, e);
        }
    }

    /**
     * Create the container.
     *
     * @return the container ID.
     */
    protected abstract String doCreateContainer();

    /**
     * Starts the already created container.
     */
    protected abstract void doStartContainer();

    // -----------------------------------------------------------------------------------------------------------------

    private void bridgeJul() {
        java.util.logging.LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
        java.util.logging.Logger.getLogger("global").setLevel(Level.FINE);
    }

    private void checkConnected() {
        if (dockerClient == null) {
            throw new IllegalStateException("Not connected! Call #connect()");
        }
    }

    private void checkNotConnected() {
        if (dockerClient != null) {
            throw new IllegalStateException("Already connected!");
        }
    }
}
