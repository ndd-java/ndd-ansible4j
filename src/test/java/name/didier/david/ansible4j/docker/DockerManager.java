package name.didier.david.ansible4j.docker;

import java.io.Closeable;
import java.io.File;

/**
 * Manage a Docker container using the REST API.
 *
 * @author ddidier
 */
public interface DockerManager
        extends Closeable {

    /**
     * Connect to the Docker server. A ping is issued to test the instance.
     */
    void connect();

    /**
     * Start the container.
     */
    void startContainer();

    /**
     * Stop the container.
     */
    void stopContainer();

    // /**
    // * @return <code>true</code> if the container is running, <code>false</code> otherwise.
    // */
    // boolean isContainerRunning();

    /**
     * Close the Docker client.
     */
    @Override
    void close();

    /**
     * @return the {@code Dockerfile} used to build the image.
     */
    File getDockerFile();

    /**
     * @return the tag of the image to use.
     */
    String getImageTag();
}
