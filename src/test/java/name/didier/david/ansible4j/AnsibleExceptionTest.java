package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

@Test(groups = UNIT)
public class AnsibleExceptionTest {

    private static final Throwable CAUSE = new Throwable();
    private static final String MESSAGE = "MESSAGE";
    private static final String FORMAT = "MESSAGE %s";
    private static final Object ARGS = "ARGS";
    private static final String MESSAGE_ARGS = "MESSAGE ARGS";

    private AnsibleException e;

    public void test_AnsibleException() {
        e = new AnsibleException();
        assertThat(e.getCause()).isNull();
        assertThat(e.getMessage()).isNull();
    }

    public void test_AnsibleException_String() {
        e = new AnsibleException(MESSAGE);
        assertThat(e.getCause()).isNull();
        assertThat(e.getMessage()).isEqualTo(MESSAGE);
    }

    public void test_AnsibleException_Throwable() {
        e = new AnsibleException(CAUSE);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isEqualTo(CAUSE.toString());
    }

    public void test_AnsibleException_String_Throwable() {
        e = new AnsibleException(MESSAGE, CAUSE);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isEqualTo(MESSAGE);
    }

    public void test_AnsibleException_Format_String() {
        e = new AnsibleException(FORMAT, ARGS);
        assertThat(e.getCause()).isNull();
        assertThat(e.getMessage()).isEqualTo(MESSAGE_ARGS);
    }

    public void test_AnsibleExceptionThrowable__Format_String() {
        e = new AnsibleException(CAUSE, FORMAT, ARGS);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isEqualTo(MESSAGE_ARGS);
    }

    public void test_AnsibleException_String_Throwable_boolean_boolean() {
        e = new AnsibleException(MESSAGE, CAUSE, true, true);
        assertThat(e.getCause()).isSameAs(CAUSE);
        assertThat(e.getMessage()).isEqualTo(MESSAGE);
    }
}
