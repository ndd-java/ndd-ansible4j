package name.didier.david.ansible4j;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgGroups.INTEGRATION;

import java.io.File;
import java.net.URL;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgResourceListener;
import name.didier.david.test4j.testng.TestResource;

@Test(groups = INTEGRATION)
@Listeners(TestNgResourceListener.class)
public class AnsiblePlaybookTest {

    @TestResource("docker/files")
    private URL dockerFilesUrl;
    @TestResource("docker/files/usr/local/ssh/without-password.rsa")
    private URL privateKeyUrl;
    @TestResource("inventory.txt")
    private URL inventoryUrl;
    @TestResource("ansible/playbook.yml")
    private URL playbookUrl;
    @TestResource("ansible/dark-playbook.yml")
    private URL darkPlaybookUrl;

    /** Used to speed up manual testing only. Should be false. */
    private final boolean externalDocker = false;

    private Ansible4jDockerManager manager;
    private AnsiblePlaybook ansiblePlaybook;

    @BeforeClass
    public void beforeClass() {
        // TODO
        if (externalDocker) {
            return;
        }

        manager = new Ansible4jDockerManager();
        manager.connect();
        manager.startContainer();
    }

    @BeforeMethod
    public void create_ansible() {
        File privateKeyFile = new File(privateKeyUrl.getPath());
        Ansible4jTestUtils.setPrivate(privateKeyFile);

        ansiblePlaybook = new AnsiblePlaybook();
        ansiblePlaybook.getOptions().setInventory(inventoryUrl.getPath());
        ansiblePlaybook.getOptions().setRemoteUser("ansible4j");
        ansiblePlaybook.getOptions().setPrivateKey(privateKeyUrl.getPath());
        // ansiblePlaybook.getOptions().setVerbosity(HIGHEST);
    }

    @AfterClass
    public void afterClass() {
        if (externalDocker) {
            return;
        }
        manager.stopContainer();
        manager.close();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void ansible_should_return_success_for_a_valid_playbook() {
        AnsibleExitCode code = ansiblePlaybook.runAndWait(new File(playbookUrl.getPath()));
        assertThat(code).isEqualTo(AnsibleExitCode.SUCCESS);
    }

    public void ansible_should_return_failed_for_an_invalid_playbook() {
        AnsibleExitCode code = ansiblePlaybook.runAndWait(new File("invalid"));
        assertThat(code).isEqualTo(AnsibleExitCode.ERROR);
    }

    public void ansible_should_return_dark_for_an_invalid_host() {
        AnsibleExitCode code = ansiblePlaybook.runAndWait(new File(darkPlaybookUrl.getPath()));
        assertThat(code).isEqualTo(AnsibleExitCode.DARK);
    }
}
