#!/bin/bash

DOCKER_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

IMAGE_NAME="ddidier/ndd-ansible4j"
CONTAINER_NAME="ndd-ansible4j"
HOST_HTTP_PORT=11080
HOST_SSH_PORT=11022


function docker-manager() {
    local action="$1"

    case $action in
        build)  _docker-manager_build;;
        run)    _docker-manager_run;;
        start)  _docker-manager_start;;
        stop)   _docker-manager_stop;;
        shell)  _docker-manager_shell;;
        logs)   _docker-manager_logs;;
        delete) _docker-manager_delete;;
        help)   _docker-manager_usage;;
        *)      _docker-manager_usage-error "No action specified" ;;
    esac
}



function _docker-manager_usage() {
    echo "Usage:"
    echo "  docker-manager build - Build the image '$IMAGE_NAME' from the Dockerfile"
    echo "  docker-manager run   - Run the container '$CONTAINER_NAME'"
    echo "  docker-manager start - Start the container '$CONTAINER_NAME'"
    echo "  docker-manager stop  - Stop the container '$CONTAINER_NAME'"
    echo "  docker-manager shell - Start a shell in the container '$CONTAINER_NAME'"
    echo "  docker-manager logs  - Fetch the logs of the container '$CONTAINER_NAME'"
    echo "  docker-manager help  - Print this message"
}

function _docker-manager_usage-error() {
    echo "ERROR: $1"
    _docker-manager_usage
    exit -1
}



function _docker-manager_build() {
    docker build -t $IMAGE_NAME .
}

function _docker-manager_run() {
    docker run -it -d                 \
               -p $HOST_SSH_PORT:22   \
               -p $HOST_HTTP_PORT:80  \
               --name=$CONTAINER_NAME \
               $IMAGE_NAME
}

function _docker-manager_start() {
    docker start $CONTAINER_NAME
}

function _docker-manager_stop() {
    docker stop $CONTAINER_NAME
}

function _docker-manager_shell() {
    docker run -it                    \
               -p $HOST_SSH_PORT:22   \
               -p $HOST_HTTP_PORT:80  \
               $IIMAGE_NAME           \
               /bin/bash
}

function _docker-manager_logs() {
    docker logs -f $CONTAINER_NAME
}

function _docker-manager_delete() {
    _docker-manager_delete-containers
    _docker-manager_delete-image
    _docker-manager_delete-dangling-images
}
function _docker-manager_delete-containers() {
    docker ps -a -q | xargs -n 1 -I {} docker rm {}
}
function _docker-manager_delete-image() {
    docker rmi $IMAGE_NAME
}
function _docker-manager_delete-dangling-images() {
    docker rmi $(docker images -qf "dangling=true")
}



docker-manager $*
