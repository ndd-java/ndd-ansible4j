package name.didier.david.ansible4j;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Wraps the native Ansible command.
 *
 * @author ddidier
 */
public class AnsiblePlaybook
        extends AbstractAnsible<AnsiblePlaybookOptions> {

    /** The default path to the Ansible Playbook executable. */
    public static final String DEFAULT_ANSIBLE_PLAYBOOK_PATH = "ansible-playbook";

    /** The options of the {@code ansible} command. */
    private final AnsiblePlaybookOptions options = new AnsiblePlaybookOptions();

    /** Constructor using the default Ansible Playbook path. */
    public AnsiblePlaybook() {
        this(DEFAULT_ANSIBLE_PLAYBOOK_PATH);
    }

    /**
     * Constructor using the given Ansible Playbook path.
     *
     * @param ansiblePlaybookPath the path to the Ansible Playbook executable.
     */
    public AnsiblePlaybook(String ansiblePlaybookPath) {
        super(ansiblePlaybookPath);
    }

    /** @return the options of the {@code ansible} command. */
    @Override
    public AnsiblePlaybookOptions getOptions() {
        return options;
    }

    /**
     * Run the given Ansible playbook.
     *
     * @param playbook the Ansible playbook to run.
     * @return the result of the command.
     */
    public AnsibleExitCode runAndWait(File playbook) {
        checkNotNull(playbook, "playbook");

        List<String> commands = new ArrayList<>();
        commands.add(scriptPath);
        commands.add(playbook.getPath());
        commands.addAll(options.toList());

        return runAndWait(commands);
    }
}
