package name.didier.david.ansible4j;

/**
 * The level of verbosity of the Ansible scripts.
 * 
 * @author ddidier
 */
public enum AnsibleVerbosity {

    /** No verbosity. */
    NONE(""),
    /** Lowest verbosity. */
    LOWEST("v"),
    /** Low verbosity. */
    LOW("vv"),
    /** High verbosity. */
    HIGH("vvv"),
    /** Highest verbosity. */
    HIGHEST("vvvv");

    /** The verbosity in the Ansible format. */
    public String arguments;

    /**
     * Default constructor.
     *
     * @param arguments the verbosity in the Ansible format.
     */
    private AnsibleVerbosity(String arguments) {
        this.arguments = arguments;
    }

    /** @return the verbosity in the Ansible format. */
    public String arguments() {
        return arguments;
    }
}
