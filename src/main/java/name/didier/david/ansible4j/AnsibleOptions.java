package name.didier.david.ansible4j;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Ansible options as found in {@code man ansible}.
 *
 * @author ddidier
 */
public class AnsibleOptions
        extends AbstractAnsibleOptions {

    /** The arguments of the module to execute ({@code -a} or {@code --args}). */
    private String arguments;
    /** The name of the module to execute ({@code -m} or {@code --module-name}). */
    private String module;

    /** Default constructor. */
    public AnsibleOptions() {
        super();
    }

    /**
     * @return the options as a list suitable to {@link ProcessBuilder}.
     */
    @Override
    public List<String> toList() {
        List<String> argumentsList = super.toList();
        append(argumentsList, "-a", arguments);
        append(argumentsList, "-m", module);
        return argumentsList;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the arguments of the module to execute. */
    public String getArguments() {
        return arguments;
    }

    /** @param arguments the arguments of the module to execute. */
    public void setArguments(String arguments) {
        this.arguments = checkNotBlank(arguments, "arguments");
    }

    /** Reset the arguments of the module to execute to its default value. */
    public void resetArguments() {
        this.arguments = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the name of the module to execute. */
    public String getModule() {
        return module;
    }

    /** @param module the name of the module to execute. */
    public void setModule(String module) {
        this.module = checkNotBlank(module, "module");
    }

    /** Reset the name of the module to execute to its default value. */
    public void resetModule() {
        this.module = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        ToStringBuilder builder = toStringBuilder();
        builder.append("arguments", arguments);
        builder.append("module", module);
        return builder.toString();
    }
}
