package name.didier.david.ansible4j;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.didier.david.commons.io.AbstractProcessStreamCollector;

/**
 * Wraps the native Ansible command.
 *
 * @author ddidier
 *
 * @param <O> the type of the options of the script.
 */
public abstract class AbstractAnsible<O extends AbstractAnsibleOptions> {

    /** The duration in milliseconds to wait after stopping the process. */
    private static final int WAIT_DURATION = 1000;

    /** The associated logger. */
    protected final Logger logger;
    /** The path to the executable. */
    protected final String scriptPath;

    /**
     * Constructor using the given script.
     *
     * @param scriptPath the path to the script.
     */
    protected AbstractAnsible(String scriptPath) {
        super();
        this.logger = LoggerFactory.getLogger(getClass());
        this.scriptPath = checkNotBlank(scriptPath, "scriptPath");
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the options of the script. */
    public abstract O getOptions();

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Starts a new Ansible process with the given arguments then wait for its termination. The output is collected and
     * printed to the {@link ProcessStreamCollector} logger.
     *
     * @param commands the commands to pass to the Ansible script.
     * @return the exit code of the Ansible script.
     */
    protected AnsibleExitCode runAndWait(List<String> commands) {
        Process process = null;
        ProcessStreamCollector collector = null;
        try {
            logger.debug("Starting Ansible with: {}", commands);
            ProcessBuilder builder = new ProcessBuilder(commands.toArray(new String[commands.size()]));
            // builder.inheritIO();
            // builder.redirectErrorStream(true);
            // builder.redirectOutput(Redirect.appendTo(log));
            process = builder.start();
            collector = new ProcessStreamCollector(process);
            collector.start();
            int result = process.waitFor();
            return AnsibleExitCode.valueOf(result);
        } catch (IOException e) {
            throw new AnsibleException("Error while running Ansible", e);
        } catch (InterruptedException e) {
            throw new AnsibleException("Error while waiting for Ansible to complete", e);
        } finally {
            // some data may remain in the stream, so waiting some time
            collector.stop(WAIT_DURATION);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Collect the {@link InputStream} of the Ansible process.
     *
     * @author ddidier
     */
    protected static class ProcessStreamCollector
            extends AbstractProcessStreamCollector {

        /** The associated logger. */
        private static final Logger logger = LoggerFactory.getLogger(ProcessStreamCollector.class);

        /**
         * Constructor using {@link StandardCharsets#UTF_8}.
         *
         * @param process the process to collect the streams from.
         */
        protected ProcessStreamCollector(Process process) {
            super(process);
        }

        /**
         * Stop the collection of the process streams after the given timeout.
         *
         * @param timeout the time to wait in milliseconds before stopping.
         */
        public void stop(int timeout) {
            logger.debug("Waiting {} ms before stopping", timeout);
            synchronized (this) {
                try {
                    wait(timeout);
                } catch (InterruptedException e) {
                    logger.error("Error while waiting", e);
                }
            }
            stop();
        }

        @Override
        protected void onOutputRead(String line) {
            logger.debug(line);
        }

        @Override
        protected void onErrorRead(String line) {
            logger.error(line);
        }
    }
}
