package name.didier.david.ansible4j;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;

import java.util.ArrayList;
import java.util.List;

/**
 * Wraps the native Ansible command.
 *
 * @author ddidier
 */
public class Ansible
        extends AbstractAnsible<AnsibleOptions> {

    /** The default path to the Ansible executable. */
    public static final String DEFAULT_ANSIBLE_PATH = "ansible";

    /** The options of the {@code ansible} command. */
    private final AnsibleOptions options = new AnsibleOptions();

    /** Constructor using the default Ansible path. */
    public Ansible() {
        this(DEFAULT_ANSIBLE_PATH);
    }

    /**
     * Constructor using the given Ansible path.
     *
     * @param ansiblePath the path to the Ansible executable.
     */
    public Ansible(String ansiblePath) {
        super(ansiblePath);
    }

    /** @return the options of the {@code ansible} command. */
    @Override
    public AnsibleOptions getOptions() {
        return options;
    }

    /**
     * Run the Ansible command on the hosts matching the given pattern.
     *
     * @param hostPattern the Ansible pattern of the hosts.
     * @return the result of the command.
     */
    public AnsibleExitCode runAndWait(String hostPattern) {
        checkNotBlank(hostPattern, "hostPattern");

        List<String> commands = new ArrayList<>();
        commands.add(scriptPath);
        commands.add(hostPattern);
        commands.addAll(options.toList());

        return runAndWait(commands);
    }
}
