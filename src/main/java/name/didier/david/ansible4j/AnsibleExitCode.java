package name.didier.david.ansible4j;

/**
 * The exit codes of the Ansible script.
 *
 * @author ddidier
 */
public enum AnsibleExitCode {

    // ----------------------------
    // WARNING: ORDER IS IMPORTANT!
    // ----------------------------

    /** This includes "no hosts matched". */
    SUCCESS,
    /** Something's wrong: wrong options, missing arguments, errors in the playbook, Ansible specific errors. */
    ERROR,
    /** An error has occurred while executing on the host. */
    FAILED,
    /** An error has occurred while contacting the host. */
    DARK;

    /**
     * @param code the return code of the Ansible script.
     * @return the associated {@link AnsibleExitCode}.
     */
    public static AnsibleExitCode valueOf(int code) {
        return AnsibleExitCode.values()[code];
    }
}
