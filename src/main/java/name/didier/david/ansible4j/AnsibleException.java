package name.didier.david.ansible4j;

import java.util.Formatter;

/**
 * Ansible root exception.
 *
 * @author ddidier
 */
public class AnsibleException
        extends RuntimeException {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * @see RuntimeException#RuntimeException()
     */
    public AnsibleException() {
        super();
    }

    /**
     * @see RuntimeException#RuntimeException(String)
     */
    public AnsibleException(String message) {
        super(message);
    }

    /**
     * @see RuntimeException#RuntimeException(Throwable)
     */
    public AnsibleException(Throwable cause) {
        super(cause);
    }

    /**
     * @see RuntimeException#RuntimeException(String, Throwable)
     */
    public AnsibleException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Syntactic sugar for message formatting. See {@link Formatter}.
     *
     * @param format the format string.
     * @param args the arguments referenced by the format specifiers in the format string.
     */
    public AnsibleException(String format, Object... args) {
        super(String.format(format, args));
    }

    /**
     * Syntactic sugar for message formatting. See {@link Formatter}.
     *
     * @param cause the cause
     * @param format the format string.
     * @param args the arguments referenced by the format specifiers in the format string.
     */
    public AnsibleException(Throwable cause, String format, Object... args) {
        super(String.format(format, args), cause);
    }

    /**
     * @see RuntimeException#RuntimeException(String, Throwable, boolean, boolean)
     */
    protected AnsibleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
