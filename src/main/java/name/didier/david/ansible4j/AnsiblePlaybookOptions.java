package name.didier.david.ansible4j;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Ansible options as found in {@code man ansible}.
 *
 * @author ddidier
 */
public class AnsiblePlaybookOptions
        extends AbstractAnsibleOptions {

    // /** The tags to skip ({@code --skip-tags}). */
    // private List<String> skippedTags;

    /** Default constructor. */
    public AnsiblePlaybookOptions() {
        super();
    }

    /**
     * @return the options as a list suitable to {@link ProcessBuilder}.
     */
    @Override
    public List<String> toList() {
        List<String> argumentsList = super.toList();
        // append(argumentsList, "--skip-tags", null);
        return argumentsList;
    }

    // -----------------------------------------------------------------------------------------------------------------

    // /** @return the path to the inventory hosts file. */
    // @Override
    // public List<String> getSkippedTags() {
    // return skippedTags;
    // }
    //
    // /** @param inventory the path to the inventory hosts file. */
    // @Override
    // public void setInventory(String inventory) {
    // this.inventory = checkNotBlank(inventory, "inventory");
    // }
    //
    // /** Reset the path to the inventory hosts file. */
    // @Override
    // public void resetInventory() {
    // this.inventory = null;
    // }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        ToStringBuilder builder = toStringBuilder();
        // builder.append("skippedTags", "TODO");
        return builder.toString();
    }
}
