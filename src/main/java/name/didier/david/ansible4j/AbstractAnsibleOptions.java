package name.didier.david.ansible4j;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;
import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;
import static name.didier.david.check4j.api.ConciseCheckers.checkStrictlyPositive;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Commons Ansible options.
 *
 * @author ddidier
 */
public abstract class AbstractAnsibleOptions {

    /** The path to the inventory hosts file ({@code -i} or {@code --inventory}). */
    private String inventory;
    /** The arguments of parallelism ({@code -f} or {@code --forks}). */
    private Integer parallelism;
    /** The path to the private key ({@code --private-key}). */
    private String privateKey;
    /** The remote user to use instead of root ({@code -u} or {@code --remote-user}). */
    private String remoteUser;
    /** The verbosity ({@code -v} or {@code --verbose}). */
    private AnsibleVerbosity verbosity = AnsibleVerbosity.NONE;

    /** Default constructor. */
    protected AbstractAnsibleOptions() {
        super();
    }

    /**
     * @return the options as a list suitable to {@link ProcessBuilder}.
     */
    public List<String> toList() {
        List<String> argumentsList = new ArrayList<>();

        append(argumentsList, "-f", parallelism);
        append(argumentsList, "-i", inventory);
        append(argumentsList, "--private-key", privateKey);
        append(argumentsList, "-u", remoteUser);

        if (verbosity != AnsibleVerbosity.NONE) {
            argumentsList.add("-" + verbosity.arguments());
        }

        return argumentsList;
    }

    /**
     * @return the options as an array suitable to {@link ProcessBuilder}.
     */
    public String[] toArray() {
        List<String> argumentsList = toList();
        return argumentsList.toArray(new String[argumentsList.size()]);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the path to the inventory hosts file. */
    public String getInventory() {
        return inventory;
    }

    /** @param inventory the path to the inventory hosts file. */
    public void setInventory(String inventory) {
        this.inventory = checkNotBlank(inventory, "inventory");
    }

    /** Reset the path to the inventory hosts file. */
    public void resetInventory() {
        this.inventory = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the arguments of parallelism. */
    public Integer getParallelism() {
        return parallelism;
    }

    /** @param parallelism the arguments of parallelism. */
    public void setParallelism(Integer parallelism) {
        this.parallelism = checkStrictlyPositive(parallelism, "parallelism");
    }

    /** Reset the arguments of parallelism to its default value. */
    public void resetParallelism() {
        this.parallelism = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the path to the private key. */
    public String getPrivateKey() {
        return privateKey;
    }

    /** @param privateKey the path to the private key. */
    public void setPrivateKey(String privateKey) {
        this.privateKey = checkNotBlank(privateKey, "privateKey");
    }

    /** Reset the path to the private key to its default value. */
    public void resetPrivateKey() {
        this.privateKey = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the remote user. */
    public String getRemoteUser() {
        return remoteUser;
    }

    /** @param remoteUser the remote user. */
    public void setRemoteUser(String remoteUser) {
        this.remoteUser = checkNotBlank(remoteUser, "remoteUser");
    }

    /** Reset the remote user to its default value. */
    public void resetRemoteUser() {
        this.remoteUser = null;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the verbosity. */
    public AnsibleVerbosity getVerbosity() {
        return verbosity;
    }

    /** @param verbosity the verbosity. */
    public void setVerbosity(AnsibleVerbosity verbosity) {
        this.verbosity = checkNotNull(verbosity, "verbosity");
    }

    /** Reset the verbosity to its default value. */
    public void resetVerbosity() {
        this.verbosity = AnsibleVerbosity.NONE;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return a {@link ToStringBuilder} filled with the commons options.
     */
    protected ToStringBuilder toStringBuilder() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        builder.append("inventory", inventory);
        builder.append("parallelism", parallelism);
        builder.append("privateKey", privateKey);
        builder.append("remoteUser", remoteUser);
        builder.append("verbosity", verbosity.arguments());
        return builder;
    }

    /**
     * Adds a key/value argument to the list only if the value is not {@code null}.
     *
     * @param argumentsList the list to append to.
     * @param argumentKey the argument key, e.g. '-f'.
     * @param argumentValue the argument value, e.g. '10'.
     * @return the same list.
     */
    protected List<String> append(List<String> argumentsList, String argumentKey, Object argumentValue) {
        if (argumentValue != null) {
            argumentsList.add(argumentKey);
            argumentsList.add(argumentValue.toString());
        }
        return argumentsList;
    }
}
